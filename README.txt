INTRODUCTION
------------
The Content cookie Lock module restricts certain roles from accessing content
on your site. Several reliable content access modules already exist, but this
module is different from them all.

We had a good reason to restrict access to content by using a cookie. The cookie
is encrypted using a value and a secret key, both the value and the secret
key need to be specified on the settings page.

If you don't have a good reason to restrict access per content node using
encrypted cookie, then you should pick another module like content access or ACL
from this list: https://www.drupal.org/node/270000.

The cookie is created via a webform exposed as a block. The webform is set to
'reload current page'. You can find the settins under the Form settings when
creating a webform. This ensures the user will see the content immediately
without getting redirected when they submit the form.

The module is light and has its own schema for ascertaining if a node is locked.


FEATURES
--------
* Display a signup form using webform module

* Display a custom message on locked pages (optionally)

* Both the signup and message can be set to visible/hidden in the content manage
  display settings.


OPTIONAL
--------
If you are going to display a signup form on locked content, then you
require the following module:
 * Webform (https://www.drupal.org/project/webform)


INSTALLATION
------------
* Install as you would normally install a contributed Drupal module. See:
  https://drupal.org/documentation/install/modules-themes/modules-7
  for further information.


CONFIGURATION
-------------
* Customize the content cookie lock settings in the administration page.


- Tick the checkboxes next to the content types that should be locked, then
     you can set nodes to private when creating or editing them.


- New fields will be created for custom message and webform block under manage
     display for the selected content types.


TROUBLESHOOTING
---------------
* If you ever have problems, make sure to go through these steps:


- Ensure that the page isn't being served from your browser's cache.
     Use CTRL+R in Windows/Linux browsers, CMD+R in Mac OS X browsers to enforce
     the browser to reload everything, preventing it from using its cache.
