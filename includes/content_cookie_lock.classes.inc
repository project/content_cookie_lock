<?php

/**
 * @file
 * Restricts access to content via cookie.
 */

namespace ContentLock; {

  /**
   * Authorisation class.
   */
  class Authorisation {

    /**
     * Returns a cookie object.
     */
    public function setAuth() {
      $cookie_value = variable_get('content_cookie_lock_cookie_value',
      '');

      if (!isset($_COOKIE['Drupal_visitor_newsletter'])
        && module_exists('encrypt')) {
        user_cookie_save(array(
          'newsletter' => $this->encryptCookie($cookie_value),
        )
        );
      }
    }

    /**
     * Encrypts the COOKIE VALUE.
     *
     * @param string $cookie_value
     *   The COOKIE VALUE.
     *
     * @return string|void
     *   Returns encrypted COOKIE VALUE.
     */
    public function encryptCookie($cookie_value) {
      return encrypt($cookie_value);
    }

  }

  /**
   * EntityLock class.
   */
  class EntityLock {

    /**
     * Determines if an entity should be locked.
     *
     * @return string
     *   Returns a lock string.
     */
    public function isLocked() {
      return $this->entityAccess();
    }

    /**
     * Determines if the module is enabled and there are content types selected.
     *
     * @param object $node
     *   The node object.
     *
     * @return bool
     *   Returns TRUE if a node should be locked otherwise returns FALSE.
     */
    protected function isPrivate($node) {
      $enabled = variable_get('content_cookie_lock_enable', 0);
      $content_types = variable_get('content_cookie_lock_content_types', array());

      if (!$node) {

        return FALSE;
      }
      elseif ($enabled
        && !empty($content_types)
        && in_array($node->type, $content_types, TRUE)
        && isset($node->private)
        && $node->private) {

        return TRUE;
      }

      return FALSE;
    }

    /**
     * Performs entity access check.
     */
    public function entityAccess(&$build = NULL) {
      $cookie_value = variable_get('content_cookie_lock_cookie_value', '');
      $node = menu_get_object();

      // Decrypts the user's cookie to compare its value with the preset value.
      if (isset($_COOKIE['Drupal_visitor_newsletter'])
          && ($this->decryptCookie($_COOKIE['Drupal_visitor_newsletter'])
            == ($cookie_value))) {

        return $this->lock = '';
      }
      if ($this->isPrivate($node)) {
        // Prepare a node wrapper to access the taxonomy field.
        try {
          $build['body']['#access'] = user_access('content cookie lock access');

          return $this->lock = 'locked';
        }
        catch (\Exception $e) {
          $message = $e->getMessage();
          watchdog('content_cookie_lock', $message, array(), WATCHDOG_ERROR);
        }
      }
    }

    /**
     * Decrypts the COOKIE VALUE.
     *
     * @param string $cookie_value
     *   The COOKIE VALUE.
     *
     * @return string|void
     *   Returns a decrypted COOKIE VALUE.
     */
    private function decryptCookie($cookie_value) {
      return decrypt($cookie_value);
    }

  }

  /**
   * Class ShowContent.
   */
  class ShowContent {

    /**
     * ShowContent Constructor.
     */
    public function __construct($entity_lock = NULL) {
      $this->entityLock = $entity_lock;
    }

    /**
     * Puts all settings variables into an array.
     *
     * @return array
     *   Returns array of variables.
     */
    protected function getConfig() {
      $configuration = array(
        'enable' => variable_get('content_cookie_lock_enable', 0),
        'cookie_value' => variable_get('content_cookie_lock_cookie_value', ''),
        'cookie_key' => variable_get('content_cookie_lock_secret_key', ''),
        'content_types' => variable_get('content_cookie_lock_content_types', array()),
        'message' => variable_get('content_cookie_lock_message', ''),
        'block' => variable_get('content_cookie_lock_block', ''),
      );

      return $configuration;
    }

    /**
     * Display a custom message or a webform block on private nodes.
     */
    public function displayContent($node) {
      $content_types = $this->getConfig()['content_types'];

      if ($this->entityLock === 'locked'
        && !user_access('content cookie lock access')
      ) {
        if ($node && in_array($node->type, $content_types, TRUE)) {
          $this->showMessage($node);
          if ($this->showBlock($node)['content']) {
            $this->showBlock($node);
          }
        }
      }
    }

    /**
     * Show a custom message on private nodes.
     *
     * @param object $node
     *   The node object parameter.
     *
     * @return array
     *   Returns the message to be added to private nodes.
     */
    protected function showMessage($node) {
      if ($this->entityLock === 'locked') {
        return $node->content['content_cookie_lock_custom_message'] = array(
          '#markup' => $this->getConfig()['message'],
          '#prefix' => '<div class="cl-message">',
          '#suffix' => '</div>',
        );
      }
    }

    /**
     * Renders a block in the node.
     *
     * @param object $node
     *   The node object parameter.
     *
     * @return mixed
     *   Invokes and returns an block to be added to private nodes.
     */
    protected function showBlock($node) {
      if ($this->getConfig()['block'] != 'Not required') {
        $block_delta = $this->getConfig()['block'];
        $block = module_invoke('webform', 'block_view', $block_delta);
        $node->content['content_cookie_lock_block'] = array(
          '#markup' => render($block['content']),
          '#prefix' => '<div class="cl-block">',
          '#suffix' => '</div>',
        );

        return $block;
      }
    }

    /**
     * Prepares an array for use in hook_field_extra_fields().
     */
    public function createFields() {
      $content_types = $this->getConfig()['content_types'];
      $enabled = $this->getConfig()['enable'];

      if ($enabled && !empty($content_types)) {
        foreach ($content_types as $type) {
          if ($type) {
            $extra['node'][$type]['display'] = array(
              'content_cookie_lock_custom_message' => $this->fieldMessage(),
              'content_cookie_lock_block' => $this->fieldBlock(),
            );
          }
        }

        return $extra;
      }

      return NULL;
    }

    /**
     * Prepares the message field display.
     *
     * @return array
     *   Returns array of field properties.
     */
    protected function fieldMessage() {
      $field_message = array(
        'label' => t('Content Cookie Lock Custom Message'),
        'description' => t('A message shown to users who are locked out.'),
        'weight' => 1,
      );

      return $field_message;
    }

    /**
     * Prepares the block field display.
     *
     * @return array
     *   Returns array of field properties.
     */
    protected function fieldBlock() {
      $field_block = array(
        'label' => t('Content Cookie Lock block'),
        'description' => t('A block shown to users who are locked out.'),
        'weight' => 2,
      );

      return $field_block;
    }

  }

  /**
   * Class DB.
   */
  class DB {

    /**
     * Prepares the settings variables.
     */
    protected function config() {
      $config = array(
        'enabled' => variable_get('content_cookie_lock_enable', 0),
        'content_types' => variable_get('content_cookie_lock_content_types', array()),
        'webform_block' => variable_get('content_cookie_lock_block', ''),
      );

      return $config;
    }

    /**
     * Determines if a node can be set to private.
     */
    protected function isAllowed($node = NULL) {
      $content_types = $this->config()['content_types'];

      if ($node && $this->config()['enabled'] && !empty($content_types)) {
        // Only include on selected content types.
        if (in_array($node->type, $content_types, TRUE)) {

          return TRUE;
        }
      }

      return FALSE;
    }

    /**
     * Finds out if there is already a record in the database.
     *
     * @param object $node
     *   The node object.
     *
     * @return bool
     *   Returns TRUE if there is a record otherwise returns FALSE.
     */
    protected function dbExists($node) {
      if ($this->isAllowed($node)) {
        $exists = db_query(
          'SELECT nid
          FROM {content_cookie_lock}
          WHERE nid = :nid',
          array(':nid' => $node->nid)
        )->fetchField();

        // If there is already a record, return TRUE.
        if ($exists) {

          return TRUE;
        }
      }

      return FALSE;
    }

    /**
     * Updates the record in the content_cookie_lock table if it exists.
     *
     * If the record doesn't exist, creates it.
     */
    public function dbUpdate($node) {
      if ($this->isAllowed($node)) {
        // If there is already a record, update it with the new private value.
        if ($this->dbExists($node)) {
          $num_updated = db_update('content_cookie_lock')
            ->fields(array(
              'nid' => $node->nid,
              'private' => !empty($node->private) ? 1 : 0,
            ))
            ->condition('nid', $node->nid)
            ->execute();
        }
        // Otherwise, create a new record.
        else {
          $this->dbInsert($node);
        }
      }
    }

    /**
     * Gathers and adds the private setting for the nodes Drupal is loading.
     */
    public function dbQuery($nodes) {
      $result = db_query('
        SELECT nid, private
        FROM {content_cookie_lock}
        WHERE nid IN(:nids)',
        array(':nids' => array_keys($nodes))
      );

      foreach ($result as $record) {
        $nodes[$record->nid]->private = $record->private;
      }
    }

    /**
     * Deletes the content_cookie_lock record when the node is deleted.
     */
    public function dbDelete($node) {
      db_delete('content_cookie_lock')
        ->condition('nid', $node->nid)
        ->execute();
    }

    /**
     * Inserts a new access record when a node is created.
     */
    public function dbInsert($node) {
      if (isset($node->private)) {
        db_insert('content_cookie_lock')->fields(
          array(
            'nid' => $node->nid,
            'private' => (int) $node->private,
          )
        )->execute();
      }
    }

  }

  /**
   * Class ContentInstance.
   */
  class ContentInstance {

    protected $node;

    /**
     * ContentInstance constructor.
     */
    public function __construct($node) {
      $this->node = $node;
    }

    /**
     * Instantiates classes for use in hooks.
     *
     * @param object $node
     *   The node object.
     */
    public static function getInstance($node) {
      try {
        $lock = new EntityLock();
        $content = new ShowContent($lock->isLocked());
        $content->displayContent($node);
      }
      catch (\Exception $e) {
        $message = $e->getMessage();
        watchdog('content_cookie_lock', $message, array(), WATCHDOG_ERROR);
      }
    }

  }
}
