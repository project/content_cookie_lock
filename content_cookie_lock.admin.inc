<?php

/**
 * @file
 * Restricts access to content via cookie.
 */

/**
 * Implements hook_form().
 */
function content_cookie_lock_admin_settings($form, &$form_state) {
  $content_types = node_type_get_names();
  $webform_blocks = webform_blocks_list();
  $default_message = t('Welcome to our site. To view the content on this page
   you have to be a newsletter member. Sign the form below to obtain access.');

  $form['global_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Global Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['global_fieldset']['content_cookie_lock_enable'] = array(
    '#type' => 'checkbox',
    '#default_value' => variable_get('content_cookie_lock_enable', 0),
    '#title' => '<strong>' . t('Enable Content Lock on this site.') .
    '</strong>',
    '#description' => t('Must be enabled to start restricting access on
    this_site.'),
  );
  $form['global_fieldset']['content_cookie_lock_cookie_value'] = array(
    '#type' => 'textfield',
    '#default_value' => variable_get('content_cookie_lock_cookie_value', ''),
    '#title' => t('Cookie Value'),
    '#description' => t('Specify the cookie value that will be encrypted.'),
    '#required' => TRUE,
    '#size' => 40,
    '#maxlength' => 128,
  );
  $form['content_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Settings for the content types'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['content_fieldset']['content_cookie_lock_content_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Content Types'),
    '#description' => t('Select the content types for which you want to
    restrict accss.'),
    '#default_value' => variable_get('content_cookie_lock_content_types',
      array()),
    '#options' => $content_types,
    '#required' => TRUE,
  );
  $form['show_content_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Settings for showing a message and webform block'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['show_content_fieldset']['content_cookie_lock_message'] = array(
    '#type' => 'textarea',
    '#title' => t('Message'),
    '#description' => t('Show a short message on restricted pages, e.g. you
    do not have permission to view content on this page.'),
    '#default_value' => variable_get('content_cookie_lock_message',
      $default_message),
  );
  $form['show_content_fieldset']['content_cookie_lock_block'] = array(
    '#type' => 'select',
    '#title' => t('Webform Blocks'),
    '#description' => t('Select a webform block if you want to show a sign up
     form (optional). You will need to first create a webform and expose it as
     a block.'),
    '#default_value' => variable_get('content_cookie_lock_block', ''),
    '#options' => $webform_blocks,
  );

  return system_settings_form($form);
}

/**
 * Helper function gets a list of blocks.
 *
 * @return mixed
 *   Returns an array of webform block names keyed by the block delta.
 */
function webform_blocks_list() {
  global $theme_key;

  module_load_include('inc', 'block', 'block.admin');
  $blocks = block_admin_display_prepare_blocks($theme_key);

  foreach ($blocks as $block) {
    if ($block['module'] == 'webform') {
      if ($block) {
        $webform_blocks[$block['delta']] = $block['info'];
      }
    }
  }

  // Add a not required option to the top of the array.
  $webform_blocks = array_merge(array('Not required' => 'Not required'),
    $webform_blocks);

  return $webform_blocks;
}
